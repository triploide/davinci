<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Categorías</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tailwindcss/2.0.1/tailwind.min.css">
</head>
<body>
    <h1>Crear Categoría</h1>

    <form action="/categories" method="POST">
        @csrf
        <div>
            <label for="name">Nombre</label>
            <input type="text" name="name">
        </div>
        <div>
            <label for="rating">Rating</label>
            <input type="text" name="rating">
        </div>
        <div>
            <label for="order">Orden</label>
            <input type="text" name="order">
        </div>
        <button type="submit">Guardar</button>
    </form>
</body>
</html>