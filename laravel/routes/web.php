<?php

use App\Http\Controllers\CategoryController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('peliculas', function () {
    return view('peliculas');
});

Route::get('peliculas/{id}', function ($id) {
    //dd($id);
    $data = [
        'id' => $id,
        'fecha' => date('d-m-Y')
    ];

    return view('pelicula', $data);
});

Route::get('categories', [CategoryController::class, 'index']);

// Route::get('peliculas/{id}/{otraCosa}', function ($id, $otraCosa) {
//     return view('pelicula');
// });

Route::get('categories/create', [CategoryController::class, 'create']);
Route::post('categories', [CategoryController::class, 'store']);