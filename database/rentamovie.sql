-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.6-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para rentamovie
CREATE DATABASE IF NOT EXISTS `rentamovie` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `rentamovie`;

-- Volcando estructura para tabla rentamovie.generos
CREATE TABLE IF NOT EXISTS `generos` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla rentamovie.generos: ~6 rows (aproximadamente)
DELETE FROM `generos`;
/*!40000 ALTER TABLE `generos` DISABLE KEYS */;
INSERT INTO `generos` (`id`, `nombre`) VALUES
	(1, 'Acción'),
	(2, 'Comedia'),
	(3, 'Drama'),
	(4, 'Infantiles'),
	(5, 'Suspenso'),
	(6, 'Terror');
/*!40000 ALTER TABLE `generos` ENABLE KEYS */;

-- Volcando estructura para tabla rentamovie.peliculas
CREATE TABLE IF NOT EXISTS `peliculas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `banner` varchar(50) NOT NULL,
  `estreno` date DEFAULT NULL,
  `genero_id` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `genero_id` (`genero_id`),
  CONSTRAINT `peliculas_genero_id` FOREIGN KEY (`genero_id`) REFERENCES `generos` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla rentamovie.peliculas: ~13 rows (aproximadamente)
DELETE FROM `peliculas`;
/*!40000 ALTER TABLE `peliculas` DISABLE KEYS */;
INSERT INTO `peliculas` (`id`, `titulo`, `banner`, `estreno`, `genero_id`) VALUES
	(1, 'Batman: El caballero de la noche', 'batman.jpg', '2008-07-18', 1),
	(2, 'Borat ', 'borat.jpg', '2006-11-03', 2),
	(3, 'Casi Famosos', 'casi-famosos_.jpg', '2000-09-22', 3),
	(4, 'Gladiador', 'gladiador.jpg', '2000-05-05', 1),
	(5, 'Gravity', 'gravity.jpg', '2013-10-04', 1),
	(6, 'Inception', 'inception.jpg', '2010-07-16', 1),
	(7, 'John Wick', 'john-wick.jpg', '2004-10-24', 1),
	(8, 'Joker', 'joker.jpg', '2019-10-04', 1),
	(9, 'Los increíbles', 'los-increíbles.jpg', '2004-11-05', 4),
	(10, 'Mad Max: furia en la carretera', 'mad-max.jpg', '2015-05-15', 1),
	(11, 'Memento', 'memento.jpg', '2001-05-21', 5),
	(12, 'Ted', 'ted.jpg', '2012-06-29', 2),
	(13, 'WALL-E', 'wall-e.jpg', '2008-06-27', 4);
/*!40000 ALTER TABLE `peliculas` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
