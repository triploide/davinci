<?php

require 'autoload.php';

$oro = new Oro(50);
$piedra = new Piedra(30);

$aldeano = new Aldeano('Maya');
$aldeano->recolectar($piedra);
$aldeano->recolectar($oro);

$piquero = new Piquero('Persa');
$piquero->atacarA($aldeano);

