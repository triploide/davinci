<?php

abstract class Personaje
{
    protected $civi;
    protected $vida;

    public function __construct($civi)
    {
        $this->civi = $civi;
        $this->vida = 100;
    }

    public function getVida()
    {
        return $this->vida;
    }

    public function setVida($vida)
    {
        $this->vida = $vida;
    }
}
