<?php

class Aldeano
{
    private $x;
    private $y;

    public function __construct($x, $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    public function mover($x, $y)
    {
        // $this->x; //10
        // $x;// 10

        while( ($x > $this->x || $y > $this->y) || ($x < $this->x || $y < $this->y) ) {
            if ($x > $this->x) {
                $this->x++;
            } elseif ($x < $this->x) {
                $this->x--;
            }

            if ($y > $this->y) {
                $this->y++;
            } elseif ($y < $this->y) {
                $this->y--;
            }
        }
    }
}
