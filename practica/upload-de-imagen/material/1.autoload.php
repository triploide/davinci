<?php

/**
 * El autoloader está destinado a cargar de forma automática las clases utilizadas. Cada vez que se intenta inicializar una clase y la clase no existe, el nombre de esta clase se pasa al autoloader y este es ejecutado.
 * 
 * En el autoloader podremos automatizar el proceso de carga sin tener que incluir manualmente cada archivo y además nos permite hacer el código más rápido, pues sólo se cargarán las clases que efectivamente se utilicen.
 */

spl_autoload_register(function ($object) {
    require "classes/$object.php";
});
