<?php

require __DIR__ . '/../../../conn.php';

$query = $db->prepare('SELECT * FROM peliculas WHERE id = :id');
$query->execute([
    ':id' => $_GET["id"]
]);

$pelicula = $query->fetch(PDO::FETCH_OBJ);
