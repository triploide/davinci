<?php
session_start();

require __DIR__ . '/../../../conn.php';

// Cargo la imagen sólo si el usuario la adjunto en el form
if (isset($_FILES['banner']) && $_FILES['banner']['tmp_name']) {

    // Seteo el directorio donde voy a gardar la imagen y le asigno un nombre
    $destino = __DIR__ . '/../../../../images/peliculas/'; // directorio
    $nombre = uniqid(); // nombre
    $ext = pathinfo($_FILES['banner']['name'], PATHINFO_EXTENSION); //extensión
    $banner = "$nombre.$ext";

    // Guardo la imagen
    move_uploaded_file($_FILES['banner']['tmp_name'], "$destino$banner");
}

$query = $db->prepare('UPDATE peliculas SET titulo=:titulo, estreno=:estreno, banner=:banner WHERE id = :id');

$query->execute([
    ':id' => $_POST["id"],
    ':titulo' => $_POST["titulo"],
    ':estreno' => $_POST["estreno"],
    ':banner' => $banner // Guardo en la base el nombre (sin el path)
]);

$_SESSION['alert'] = [
    'message' => 'La película se editó con éxito',
    'type' => 'success'
];

header('location: /davinci/practica/upload-de-imagen/admin/peliculas/index.php');
