<?php

if (isset($_POST['ejemplo'])) {
    if (is_numeric($_POST['ejemplo'])) {
        header('location: /10-patron-mvc/ejemplo/success.php');
    } else {
        header('location: /10-patron-mvc/ejemplo/error.php');
    }

} else {
    header('location: /10-patron-mvc/ejemplo/error.php');
}
