<?php

require __DIR__ . '/../conn.php';
require __DIR__ . '/../models/Pelicula.php';

$pelicula = new Pelicula($db);
$peliculas = $pelicula->search('titulo', $_GET['buscar']);

require __DIR__ . '/../../peliculas.php';