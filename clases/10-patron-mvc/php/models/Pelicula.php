<?php

class Pelicula
{
    private $db;
    private $id;
    private $titulo;
    private $banner;
    private $estreno;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitulo()
    {
        return $this->titulo;
    }

    public function getBanner()
    {
        return $this->banner;
    }

    public function getEstreno()
    {
        return $this->estreno;
    }

    // find($id)

    // all()

    // search($options)

    public function search($column, $search)
    {
        $sql = 'SELECT * FROM peliculas where '. $column .' like "'. $search .'%"';
        $query = $this->db->prepare($sql); //PDOStatement
        $query->execute();
        return $query->fetchAll(PDO::FETCH_OBJ);
    }
}

