<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <form method="POST" action="/10-patron-mvc/php/controllers/form.controller.php">
        <p>
            <input type="text" name="ejemplo" value="">
        </p>

        <p>
            <button type="submit" value="Enviar">Enviar</button>
        </p>
    </form>
</body>
</html>