<?php

require __DIR__ . '/../conn.php';
require __DIR__ . '/../models/Genero.php';

$modelGenero = new Genero($db);

$generos = $modelGenero->all();

require __DIR__ . '/../../views/generos.php';
