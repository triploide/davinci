<?php

require __DIR__ . '/../conn.php';
require __DIR__ . '/../models/Genero.php';

$modelGenero = new Genero($db);

$genero = $modelGenero->find($_GET['id']);

require __DIR__ . '/../../views/genero.php';
