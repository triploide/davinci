<?php

class Genero
{
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }
    
    public function all(): array
    {
        $query = $this->db->prepare('SELECT * FROM generos');

        $query->execute();

        return $query->fetchAll(PDO::FETCH_OBJ);
    }

     public function find(int $id)
     {
         $query = $this->db->prepare('SELECT * FROM generos where id = :id');
 
         $query->execute([
             'id' => $id
         ]);
 
         return $query->fetch(PDO::FETCH_OBJ);
     }

}