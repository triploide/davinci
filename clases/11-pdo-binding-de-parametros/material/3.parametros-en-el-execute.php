<?php

/**
 * La otra forma de indicar el valor de los parámetros es hacerlo directamente cuando llamamos al método execute.
 * 
 * En este caso lo que vamos a hacer es pasar al método execute un parámetro del tipo array asociativo. En este array asociativo vamos a pasar como clave del array el nombre que le dimos al placeholder (incluyendo los dos puntos) y como valor, el valor que necesita recibir ese placeholder
 */

$sql = "SELECT * FROM peliculas where id = :id";
$query = $db->prepare($sql);

// Bind de parámetros en el execute
$pelicula = $query->execute([
    ':id' => $_GET['id']
]);
