<?php

/**
 * A la hora de preparar una query algunos de los datos que va a recibir pueden tener un valor dinámico. Por ejemplo, podemos tener una query que recupere un película según su id y este id puede provenir de un dato enviado por usuario y que nos llega a nosotros por GET o POST (u otro mecanismo).
 * 
 * Una forma de resolver esto es insertar estos valores dinámicos dentro de la consulta SQL usando la concatenación. En esta aproximación si el dato que pasamos está mal sanitizado nuestra consulta queda vulnerable al SQL Injection.
 * 
 * PDO nos brinda una solución mejor a este problema, a través de las consultas parametrizadas.
 * 
 * Una consulta parametrizada es una query espera valores que aun no hemos definido, pero que vamos a pasar a la hora de ejecutarla.
 * 
 * Los parámetros en la consuta se definen utilizando dos puntos y luego una cadena de texto con el nombre del parámeto.
 * 
 * El nombre que le demos al parámetro nos servirá a nosotors para después identificarlo.
 */

$sql = "SELECT * FROM peliculas where id = :id";
$query = $db->prepare($sql);
