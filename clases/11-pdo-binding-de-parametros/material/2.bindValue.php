<?php

/**
 * Hay dos formas de pasar los valores a la consulta con parámetros.
 * Podemos utilizar el método bindValue del objeto query. Este método recibe tres parámetros:

	El identificador del parámetro. Es decir, el nombre que le pusimos al placeholder en la query (incluyendo los dos puntos)
	El valor. El valor que le vamos a pasar al parámetro.
	El tipo de dato. Indica el tipo que debe tener el valor pasado en el segundo parámetro del método. Para indicar esto se debe utilizar las constantes de PDO. Ver: https://www.php.net/manual/es/pdo.constants.php

 */

$sql = "SELECT * FROM peliculas where id = :id";
$query = $db->prepare($sql);

// Bind Value
$query->bindValue(':id', $_GET['genero_id'], PDO::PARAM_INT);
