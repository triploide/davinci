<?php

/**
 * PDO::FETCH_CLASS: devuelve una nueva instancia de la clase solicitada, haciendo corresponder las columnas del conjunto de resultados con los nombres de las propiedades de la clase.
 * 
 * PDO::FETCH_CLASS puede usarse tanto con el método fetch como con el método fetchAll.
 * 
 * Para ambos casos es necesario pasar como segundo parámetro (de fetch o fetchAll) el nombre completamente calificado de la clase que se va a usar para almacenar el resultado de la consulta.
 */

// Trae un array de objetos Genero
$generos = $query->fetchAll(PDO::FETCH_CLASS, 'Genero');

// Trae un único objeto Genero
$generos = $query->fetch(PDO::FETCH_CLASS, 'Genero');
