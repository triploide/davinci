<?php
require __DIR__ . '/php/conn.php';

$query = $db->prepare('SELECT id, titulo FROM peliculas');
$query->execute();

$peliculas = $query->fetchAll(PDO::FETCH_OBJ);

// echo '<pre>';
// var_dump($peliculas);
// exit;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Películas</h1>

    <?php foreach($peliculas as $pelicula): ?>
        <p><?php echo $pelicula->titulo ?></p>
    <?php endforeach; ?>

</body>
</html>