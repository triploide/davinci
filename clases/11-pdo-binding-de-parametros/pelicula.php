<?php
require __DIR__ . '/php/conn.php';

$query = $db->prepare('SELECT * FROM peliculas where id > :id and genero_id = :genero_id');

$query->execute([
    ':id' => 7,
    ':genero_id' => 1
]);

$peliculas = $query->fetchAll(PDO::FETCH_OBJ);

echo '<pre>';
var_dump($peliculas);

echo '<hr>';

$query->execute([
    ':id' => 3,
    ':genero_id' => 5
]);

$peliculas = $query->fetchAll(PDO::FETCH_OBJ);

echo '<pre>';
var_dump($peliculas);
exit;

// $query->execute([
//     ':id' => $_GET['id']
// ]);

//$query->bindValue(':id', $_GET['id'], PDO::PARAM_INT);

$query->execute();

$pelicula = $query->fetch(PDO::FETCH_OBJ);

// echo '<pre>';
// var_dump($peliculas);
// exit;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1><?php echo $pelicula->titulo ?></h1>
</body>
</html>