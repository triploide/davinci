<?php
session_start();

require __DIR__ . '/../../../conn.php';

$query = $db->prepare('UPDATE peliculas SET titulo=:titulo, estreno=:estreno WHERE id = :id');

$query->execute([
    ':id' => $_POST["id"],
    ':titulo' => $_POST["titulo"],
    ':estreno' => $_POST["estreno"],
]);

$_SESSION['alert'] = [
    'message' => 'La película se editó con éxito',
    'type' => 'success'
];

header('location: /12-AMB/admin/peliculas/index.php');
