<?php

require 'php/conn.php';

$query = $db->prepare('SELECT * FROM peliculas'); // PDOStatament
$query->execute();

$peliculas = $query->fetchAll(PDO::FETCH_OBJ);

// echo '<pre>';

// var_dump($peliculas);

foreach($peliculas as $pelicula) {
    echo $pelicula->titulo . '<br>';
}
