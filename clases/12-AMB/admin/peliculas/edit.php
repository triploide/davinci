<?php require __DIR__ . '/../../php/controllers/admin/peliculas/edit.controller.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Películas</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
</head>
<body>
    <div class="container py-5">
        <div class="row">
            <div class="col-md-12">
                <form action="/12-AMB/php/controllers/admin/peliculas/update.controller.php" method="POST">
                    <div class="form-group">
                        <label for="titulo">Título</label>
                        <input type="text" class="form-control" id="titulo" name="titulo" value="<?php echo $pelicula->titulo ?>">
                    </div>
                    <div class="form-group">
                        <label for="estreno">Fecha de estreno</label>
                        <input type="date" class="form-control" id="estreno" name="estreno" value="<?php echo $pelicula->estreno ?>">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Enviar</button>
                    </div>
                    <input type="hidden" name="id" value="<?php echo $pelicula->id; ?>">
                </form>
            </div>
        </div>
    </div>
</body>
</html>