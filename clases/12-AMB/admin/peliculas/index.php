<?php
session_start();
require __DIR__ . '/../../php/controllers/admin/peliculas/index.controller.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Películas</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <?php if (isset($_SESSION['alert']) && $_SESSION['alert']['message']): ?>
                    <div class="alert alert-<?php echo $_SESSION['alert']['type'] ?>">
                        <?php echo $_SESSION['alert']['message']; ?>
                    </div>
                <?php unset($_SESSION['alert']); ?>
                <?php endif; ?>

                <table class="table table-bordered striped">
                    <thead>
                        <tr>
                            <td>Título</td>
                            <td>Acciones</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($peliculas as $pelicula): ?>
                            <tr>
                                <td><?php echo $pelicula->titulo; ?></td>
                                <td>
                                    <a href="/12-AMB/admin/peliculas/edit.php?id=<?php echo $pelicula->id; ?>" class="btn btn-primary"><span class="fa fa-edit"></span></a>
                                    <a href="#" class="btn btn-danger"><span class="fa fa-trash"></span></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>