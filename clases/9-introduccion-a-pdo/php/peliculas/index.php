<?php
require __DIR__ . '/../conn.php';

// preparar
if (isset( $_GET['genero_id'])) {
    $sql = 'SELECT * FROM peliculas where genero_id = ' . $_GET['genero_id'];
} else {
    $sql = 'SELECT * FROM peliculas';
}

$query = $db->prepare($sql); //PDOStatement

// ejecutar
$query->execute();

// recuperar

// --- fetchAll ---
// $peliculas = $query->fetchAll(PDO::FETCH_ASSOC);
$peliculas = $query->fetchAll(PDO::FETCH_OBJ);

// --- fetch ---
// $peliculas = $query->fetch(PDO::FETCH_ASSOC);
// $pelicula1 = $query->fetch(PDO::FETCH_OBJ);
// $pelicula2 = $query->fetch(PDO::FETCH_OBJ);

// --- fetchColumn ---

// $sql2 = 'SELECT titulo FROM peliculas where id = 1';
// $query2 = $db->prepare($sql2); //PDOStatement
// $query2->execute();

// $titulo = $query2->fetchColumn();

// var_dump($titulo);


