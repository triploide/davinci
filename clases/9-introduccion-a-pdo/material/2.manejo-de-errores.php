<?php

/**
 * Al momento de crear la instancia de la clase PDO podemos setear que estrategia para el manejo de errores queremos usar. Esto lo podemos hacer a través el método setAttribute de la clase PDO.
 * 
 * El primer parámetro del método setAttribute es el nombre atributo y el segundo el valor a settear.
 * 
 * El nombre del atributo lo recuperamos de la constante ATTR_ERRMODE.
 * 
 * Para el valor existen tres posibilidades: 
 *      PDO::ERRMODE_SILENT, las consultas con errores de mysql no arrojaran errores en php
 *      PDO::ERRMODE_WARNING, las consultas con errores de mysql emitirán un mensaje E_WARNING en php (que no interrumpe el flujo de la aplicación)
 *      ERRMODE_EXCEPTION, las consultas con errores de mysql lanzarán una PDOException
 */

try {
    $db = new PDO('mysql:dbname=nombreDelaBase;charset=UTF8', 'root', '');
    // Seteo del nivel de manejo de errores
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo $e->getMessage();
}
