<?php

/**
 * La ejecución de una consulta en PDO consta de tres pasos: "definir, ejecutar y obtener".
 * 
 * Para definir la query utilizamos el método prepare de la clase PDO. Como parámetro pasamos nuestra consulta SQL. Este método nos devuelve un objeto PDOStatement.
 * 
 * Luego invocamos al método execute del objeto PDOStatement.
 * 
 * Finalmente obtenemos la respuesta llamando a algunos de los siguientes métodos del objeto PDOStatement:
 *      fetch (recupera un único registro)
 *      fetchAll (recupera todos los registros de la consulta)
 *      fetchColumn (recupera una única columna de un único registro)
 *      rowCount (devuelve el número de fila afectadas en la consulta)
 * 
 * A su vez, los métodos fetch y fecthAll aceptan cómo parámetro el "fetch_style" que indica el formato en que se debe devolver la información recuperada:
 *      PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
 *      PDO::FETCH_OBJ: devuelve un objeto anónimo con nombres de propiedades que se corresponden a los nombres de las columnas devueltas en el conjunto de resultados. 
 * 
 * NOTA: los método fetch y fetch column puede seguir ejecutándose sobre una misma consulta e irán devolviendo el segundo registro, luego el tercero y así sucesivamente
 */

 // ----- FETCH ALL -----
$query = $db->prepare('SELECT * FROM TABLA');
$query->execute();
$resultado = $query->fetch(PDO::FETCH_OBJ); // Recupera todas las filas de la consulta

// ----- FETCH -----
$query = $db->prepare('SELECT * FROM TABLA');
$query->execute();
$resultado = $query->fetch(PDO::FETCH_OBJ); // Recupera la primera fila
$resultado = $query->fetch(PDO::FETCH_OBJ); // Recupera la segunda fila

// ----- FETCH COLUMN -----
$query = $db->prepare('SELECT * FROM TABLA');
$query->execute();
$resultado = $query->fetchColumn(); // Recupera la primera columna de la primera fila
$resultado = $query->fetchColumn(); // Recupera la segunda columna de la primera fila
