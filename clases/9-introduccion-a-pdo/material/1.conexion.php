<?php

/**
 * Para crear la conexión con la base de datos es necesario crear una instancia de la clase PDO
 * La clase PDO representa una conexión entre PHP y un servidor de bases de datos
 * 
 * El constructor recibe tres parámetros: DSN, usuario y contraseña:
 *      DSN: el DSN (Data Source Name) consiste en el nombre del controlador (mysql en nuestro caso) seguido por dos puntos, seguido por una secuencia de "clave=valor;". Las clave/valor que son necesarias para nosotros son el dbname y el chartset.
 *      usuario: es el usuario que se va a conectar a la base de datos (root para la mayoría de los casos).
 *      contraseña: el password de dicho usuario.
 * 
 * La creación de la instancia lanza una PDOException si el intento de conexión a la base de datos requerida falla. Por lo que es recomendable encerrar la creación del objeto en un bloque try catch.
 * 
 */

try {
    $db = new PDO('mysql:dbname=nombreDelaBase;charset=UTF8', 'root', '');
} catch (PDOException $e) {
    // Manejo del error
}
