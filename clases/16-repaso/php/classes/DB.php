<?php

class DB
{
    public static function getConn()
    {
        $db = null;

        try {
            $db = new PDO('mysql:dbname=rentamovie;host=localhost;charset=utf8', 'root', '');
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return $db;
    }
}
