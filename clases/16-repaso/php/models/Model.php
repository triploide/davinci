<?php

abstract class Model
{
    private $db;

    public function __construct()
    {
        $this->db = DB::getConn();
    }

    public abstract function getTable();

    public function all()
    {
        $query = $this->db->prepare('SELECT * FROM ' . $this->getTable());

        $query->execute();

        return $query->fetchAll(PDO::FETCH_OBJ);
    }

    public function find($id)
    {
        $query = $this->db->prepare('SELECT * FROM '. $this->getTable() .' WHERE id = :id');

        $query->execute([
            ':id' => $id
        ]);

        return $query->fetch(PDO::FETCH_OBJ);
    }
}
