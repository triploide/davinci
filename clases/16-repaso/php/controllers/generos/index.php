<?php

require __DIR__  . '/../../classes/DB.php';
require __DIR__  . '/../../classes/Config.php';
require __DIR__  . '/../../models/Model.php';
require __DIR__  . '/../../models/Genero.php';

// ir al modelo
$model = new Genero;
$generos = $model->all();
$config = Config::getConfig();

// if ($config['env'] == 'local') {
//     echo '<pre>';
//     var_dump($generos);
// }

// cargar la vista
require __DIR__ . '/../../../views/generos/index.php';
