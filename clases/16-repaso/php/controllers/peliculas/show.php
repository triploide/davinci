<?php

require __DIR__  . '/../../classes/DB.php';
require __DIR__  . '/../../classes/Config.php';
require __DIR__  . '/../../models/Model.php';
require __DIR__  . '/../../models/Pelicula.php';

// ir al modelo
$model = new Pelicula;
$pelicula = $model->find($_GET['id']);
$config = Config::getConfig();

// if ($config['env'] == 'local') {
//     echo '<pre>';
//     var_dump($pelicula);
// }

// cargar la vista
require __DIR__ . '/../../../views/peliculas/show.php';
