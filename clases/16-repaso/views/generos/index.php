<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Géneros</title>
</head>
<body>
    <h1>Géneros</h1>

    <?php foreach($generos as $genero): ?>
        <h3>
            <a href="<?php echo $config['path'] ?>genero.php?id=<?php echo $genero->id ?>">
                <?php echo $genero->nombre; ?>
            </a>
        </h3>
    <?php endforeach; ?>
</body>
</html>