<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Películas</title>
</head>
<body>
    <h1>Películas</h1>

    <?php foreach($peliculas as $pelicula): ?>
        <h3>
            <a href="<?php echo $config['path'] ?>pelicula.php?id=<?php echo $pelicula->id ?>">
                <?php echo $pelicula->titulo; ?>
            </a>
        </h3>
    <?php endforeach; ?>
</body>
</html>