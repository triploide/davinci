<?php

require __DIR__ . '/../../../php/classes/DB.php';
require __DIR__ . '/../../../php/classes/Paginator.php';
require __DIR__ . '/../models/Pelicula.php';

$cuantos = 5;

$pelicula = new Pelicula;

$peliculas = $pelicula->paginate($cuantos);

$total = $pelicula->count();

$botones = ceil($total / $cuantos);

require __DIR__ . '/../../views/peliculas.php';
