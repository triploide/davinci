<?php

class Pelicula
{
    private $db;

    public function __construct()
    {
        $this->db = DB::getConn();
    }

    public function all()
    {
        $query = $this->db->prepare('SELECT * from peliculas');

        $query->execute();

        return $query->fetchAll(PDO::FETCH_OBJ);
    }

    public function paginate($cuantos = 5)
    {
        $pagina = $_GET['pagina'] ?? 1;

        $desde = ($pagina -1) * $cuantos;

        $query = $this->db->prepare("SELECT * from peliculas limit $cuantos offset $desde");

        $query->execute();

        return $query->fetchAll(PDO::FETCH_OBJ);
    }

    public function count()
    {
        $query = $this->db->prepare('SELECT count(id) as total from peliculas');

        $query->execute();

        $resultado = $query->fetch(PDO::FETCH_OBJ);

        return $resultado->total;
    }
}

