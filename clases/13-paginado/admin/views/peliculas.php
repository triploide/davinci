<table class="table table-striped table-borderd">
    <thead>
        <tr>
            <td>Id</td>
            <td>Título</td>
            <td>Fecha de estreno</td>
        </tr>
    </thead>
    <tbody>
        <?php foreach($peliculas as $pelicula): ?>
            <tr>
                <td><?php echo $pelicula->id; ?></td>
                <td><?php echo $pelicula->titulo; ?></td>
                <td><?php echo $pelicula->estreno; ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php (new Paginator($botones, '/13-paginado/admin/peliculas.php'))->numbers(); ?>