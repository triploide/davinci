<?php

require 'vendor/autoload.php';

use Intervention\Image\ImageManager;

$manager = new ImageManager();

$image = $manager->make('images/php.jpg')->greyscale();

$image->save('images/php-gris.jpg');
