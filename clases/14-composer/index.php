<?php

require 'vendor/autoload.php';

echo '<pre>';

use Carbon\Carbon;

$fecha = Carbon::now();

echo $fecha->format('d-m-Y');

echo '<br>';

$fecha->addDays(5);

echo $fecha->format('d-m-Y');

// echo $fecha->year . '<br>';
// echo $fecha->month . '<br>';
// echo $fecha->day;
