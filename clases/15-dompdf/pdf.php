<?php

try {
    $db = new PDO('mysql:dbname=rentamovie;charset=utf8;host=localhost', 'root', '');
} catch (PDOException $e) {
    echo 'Todo mal';
}

$query = $db->prepare('SELECT * FROM peliculas');
$query->execute();
$peliculas = $query->fetchAll(PDO::FETCH_OBJ);
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PF</title>
    <style>
        <?php require 'css/bootstrap.css'; ?>
    </style>
</head>
<body>
    <h1 class="my-3">Películas</h1>

    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <td>Título</td>
                <td>Estreno</td>
            </tr>
        </thead>
        <tbody>
            <?php foreach($peliculas as $pelicula): ?>
                <tr>
                    <td><?php echo $pelicula->titulo; ?></td>
                    <td><?php echo $pelicula->estreno; ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

</body>
</html>