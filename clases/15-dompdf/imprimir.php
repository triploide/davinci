<?php

require 'vendor/autoload.php';

use Dompdf\Dompdf;

$client = new GuzzleHttp\Client();
$response = $client->request('GET', 'http://davinci.test:8888/15-dompdf/pdf.php');
$html = $response->getBody();

$dompdf = new Dompdf();
$dompdf->loadHtml($html);

$dompdf->setPaper('A4', 'landscape');

$dompdf->render();

$dompdf->stream();

$dompdf->stream("dom.pdf", ["Attachment" => false]);

